<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<header id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-light bg-secondary">
			<div class="container">
                <div class="row main">
                    <div class="col-2 d-md-none">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    </div>
                    <div class="col-md d-none d-md-block align-self-center" id="search">
                        <form action="" class="form-inline">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" placeholder="">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-primary" type="button">
                                    <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-8 col-md text-center" id="brand">
                        <a title="Veribrand" rel="home" href="/" class="brand">
                            <img src="<?php echo get_theme_file_uri('images/type.png') ?>" alt="" class="my-2">
                        </a>
                    </div>
                    <div class="col-2 col-md" id="info">
                        dsadsada
                    </div>
                </div> <!-- main row -->
                <div class="row menu">
                    <div class="col">
                        <?php wp_nav_menu(
                            array(
                                'theme_location'  => 'primary',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id'    => 'navbarNavDropdown',
                                'menu_class'      => 'navbar-nav ml-auto',
                                'fallback_cb'     => '',
                                'menu_id'         => 'main-menu',
                                'depth'           => 2,
                                'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
                            )
                        ); ?>
                    </div>
                </div>

				<!-- The WordPress Menu goes here -->
			</div><!-- .container -->
		</nav><!-- .site-navigation -->

    </header><!-- #wrapper-navbar end -->

<div class="container">
    <div class="row">
        <div class="col">
            coluna 1
        </div>
        <div class="col">
            coluna 2
        </div>
    </div>
    <div class="row">
        <div class="col">
            coluna full
        </div>
    </div>
</div>
